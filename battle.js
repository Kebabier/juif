/*
 * battle.js
 */
//Informations essentielles
var infos = {
    idbot: null,
    currentTurn: null,
    currentPoke: null,
    currentOppPoke: null
};

//Infos complémentaires pour améliorer le bot
var minorInfos = {

};

exports.Battling = {
    updatechallenges: function(c, room, data) {
        data = JSON.parse(data[2]);
        if (!data.challengesFrom) return false;
        var from = Object.getOwnPropertyNames(data.challengesFrom);
        var tier = data.challengesFrom[from];
        if (tier == 'randombattle') {
            this.talk(c, room, '/accept ' + from);
        } else {
            this.talk(c, room, '/pm ' + from + ', Désolé ' + from + ', je ne combats pas encore dans le tier ' + tier + '.');
        }
    },
    player: function(c, room, data) {
        infos.idbot = data[2];
    },
    request: function(c, room, data) {
        infos.currentPoke = data[2];
    },
    switch: function(c, room, data) {
        console.log(data)
        if (data[2].indexOf('p2a:') > -1) {
            infos.currentOppPoke = makeId(data[2].substr(5));
        }
        tools.battleEngine(infos);
    },
    turn: function(c, room, data) {
        infos.currentTurn = data[2];
    },
    win: function(c, room, data) {
        console.log(data);
        this.talk(c, room, '/leave');
    }
};
