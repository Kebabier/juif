/*
 * tools.js
 * Ce fichier contient diverses fonctions
 * utiles au moteur de combat. Le fonctionnement
 * est détaillé dans le fichier battle.js
 */
exports.Tools = {

    getWeaknesses: function(moveType, pkmnType) {
        var types = pkmnType.split('|');
        var findWeaknesses = {
            0: 1,
            1: 2,
            2: 0.5,
            3: 0
        };

        if (!types[1]) {
            return findWeaknesses[typechart[pkmnType].damageTaken[moveType]];
        } else {
            var type1 = typechart[types[0]].damageTaken[moveType];
            var type2 = typechart[types[1]].damageTaken[moveType];
            return findWeaknesses[type1] * findWeaknesses[type2];
        }
    },

    getMoveType: function(move) {
        return moves[move].type;
    },

    getPokemonType: function(pokemon) {
        pokemon = makeId(pokemon);
        return dex[pokemon].types;
    },

    randProbability: function(percent) {
        var rand = Math.floor((Math.random() * 100));
        if (percent > rand) {
            return true;
        } else {
            return false;
        }
    },

    chooseMoveAccordingToOppType: function(moves, pkmnType) {
        for (var i = 0; i < moves.length; i++) {
            var moveType = this.getMoveType(moves[i].id);
            if (this.getWeaknesses(moveType, pkmnType) > 3) {
                return moves[i].id;
            } else if (this.getWeaknesses(moveType, pkmnType) > 2) {
                return moves[i].id;
            } else if (this.getWeaknesses(moveType, pkmnType) > 1) {
                return moves[i].id;
            } else if (this.getWeaknesses(moveType, pkmnType) == 1) {
                return moves[i].id;
            } else {
                return false;
            }
        }
    },

    battleEngine: function(data) {

    }
};
